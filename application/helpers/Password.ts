import bcrypt from 'bcrypt';
import md5 from 'md5';
import sha1 from 'sha1';
type typeHash = 'md5' | 'sha1' | 'bcrypt'
class Password {
    public encode(type: typeHash, plainText: any):any {
        if(type == 'md5') {
            return md5(plainText)
        }else if(type == 'sha1') {
            return sha1(plainText)
        }else if(type == 'bcrypt') {
            return bcrypt.hash(plainText, 12);
        }
    }

    public verify(type: typeHash, plainText: any, passwordHash: any): any {
        if(type == 'md5') {
            return md5(plainText) == passwordHash
        }else if(type == 'sha1') {
            return sha1(plainText) == passwordHash
        }else if(type == 'bcrypt') {
            return bcrypt.compare(plainText, passwordHash);
        }
    }
}

export default new Password();