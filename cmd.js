const program = require('commander');
const fs      = require('fs');
const { exec }  = require('child_process')


program
    .command('make:controller')
    .description('Make a new Controller')
    .option('--name <name>', 'Controller Name')
    .action((args) => {
        let { name } = args;

        name = name.replace('Controller', '')
        name = name.replace('controller', '');
        name = name.replace('.ts', '');
        name = `${name[0].toUpperCase()}${name.slice(1)}Controller`
        if(fs.existsSync(`./application/controllers/${name}.ts`)) {
            return console.log(`Controller with class name ${name} is already exists!`);
        }
        const readFileController = fs.readFileSync(`./sample/controllers/SampleController.txt`).toString()
        const replaceClassName   = readFileController.replace(/SampleController/gi, name);

        fs.writeFile(`./application/controllers/${name}.ts`, replaceClassName, err => {
            if (err) throw new Error(err);

            console.log(`Success to create new controller with class name ${name}`);
        })

        fs.appendFile('./application/controllers/index.ts', `\r\nexport { ${name} } from './${name}'`, err => {
            if(err) throw new Error(err);

            console.log('Success to append new controller')
        })
    })

program
    .command('make:route')
    .description('Make a new route')
    .option('--name <name>', 'Route name')
    .option('--prefix <prefix>', 'Prefix route')
    .action((args) => {
        let { name, prefix } = args

        if (!prefix) return console.log('Failed to create new route');

        name = name.replace('Route', '')
        name = name.replace('route', '')
        name = name.replace('.ts', '')
        name = `${name[0].toUpperCase()}${name.slice(1)}Route`

        if (fs.existsSync(`./lib/route/${name}.ts`)) {
            return console.log('Route already exists')
        }
        const readFileRoute = fs.readFileSync(`./sample/routes/SampleRoute.txt`).toString()
        const replaceClassName = readFileRoute.replace(/SampleRoute/gi, `${name}`)
        const replacePrefixRoute = replaceClassName.replace(/\/samples/gi, `/${prefix}`)

        const readFile = fs.readFileSync('./application/routers/index.ts').toString()
        const x = readFile.split('public routes = (app: Application): void => {')
        const y = x[1].split('}')[0]
        const z = x[1].split('}')

        const writeFile = `public routes = (app: Application): void => {${y}    ${name}.routes(app)
    }
}`

        const combine = x[0] + writeFile + z[2]
        
        fs.writeFile(`./application/routers/${name}.ts`, replacePrefixRoute, err => {
            if(err) throw new Error(err)
            console.log(`Success to create new route with class name ${name}`)
        })

        fs.writeFile(`./application/routers/index.ts`, combine, (err) => {
            if (err) throw new Error(error)
        
            console.log('Success to append index route')

            fs.appendFile('./application/routers/index.ts', `\r\import ${name} from './${name}'`, (err) => {
                if (err) throw new Error(err)
            
                console.log('Success to append new route')
            })
        })

    })

program
    .command('make:model')
    .requiredOption('-a, --attributes <attributes>', 'A list of attributes')
    .requiredOption('-n, --name <name>', 'Defines the name of the new model')
    .action((args) => {
        let { attributes, name } = args;
        name = name.replace('Model', '')
        name = name.replace('model', '')
        name = name.replace('.ts', '')
        exec(`sequelize model:generate --attributes ${attributes} --name ${name} || ./node_modules/.bin/sequelize model:generate --attributes ${attributes} --name ${name}`, (error, stdout, stderr) => {
            if (error) {
                console.log(`${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        })
        
        const readFileModelSample  = fs.readFileSync(`./sample/models/SampleModel.txt`).toString()
        const replaceInterfaceName = readFileModelSample.replace(/SampleModel/gi, `${name[0].toUpperCase()}${name.slice(1)}Model`);
        const replaceID            = replaceInterfaceName.replace('id: string,', '');
        const addNewLine           = attributes.replace(',', ',\n\t');

        const x = replaceID.split(`export interface ${name[0].toUpperCase()}${name.slice(1)}Model extends Model<${name[0].toUpperCase()}${name.slice(1)}Model, any> {`)
        const y = x[1].split('}')[0]

        const writeFile = `export interface ${name[0].toUpperCase()}${name.slice(1)}Model extends Model<${name[0].toUpperCase()}${name.slice(1)}Model, any> {\n\t${addNewLine}\n}`

        const combine = x[0] + writeFile;

        fs.writeFile(`./application/resources/interfaces/models/${name}.ts`, combine, err => {
            if(err) throw new Error(err);
            console.log('Success create new model');
        })

        const readFile = fs.readFileSync('./application/models/index.ts').toString()
        const Combine = `${readFile}\nimport { ${name[0].toUpperCase()}${name.slice(1)}Model } from '../resources/interfaces/models/${name[0].toUpperCase()}${name.slice(1)}'\nexport const ${name}: ${name[0].toUpperCase()}${name.slice(1)}Model = models.${name[0].toLowerCase()}${name.slice(1)};`
        fs.writeFile(`./application/models/index.ts`, Combine, err => {
            if(err) throw new Error(err)
            console.log(`Success appending model ${name}`)
        })
    })

program.parse(process.argv)