import express, {Application, Request, Response} from 'express';

// Library
import bodyParser from 'body-parser';
import morgan from 'morgan';
import compression from 'compression';
import helmet from 'helmet'
import cors from 'cors'

import IndexRoute from './application/routers/index';

class App {
    public app: Application;
    public port: number = 8080;
    private IndexRoute: IndexRoute = new IndexRoute();

    constructor() {
        this.app = express();
        this.config();
        this.middleware();
        this.routes();
        this.listen()
    }

    protected routes(): void {
        this.IndexRoute.routes(this.app)
    }

    private config(): void {
        const { app } = this;

        app.use(bodyParser.urlencoded({extended: false}));
        app.use(bodyParser.json());
        app.use(morgan("dev"));
        app.use(compression());
        app.use(helmet());
        app.use(cors())
    }

    private middleware(): void {
        const { app } = this;
    }

    private listen(): void {
        this.app.listen(this.port, () => console.log(`This app running on port :${this.port}`))
    }
}

new App()